package com.hobomaster.gallery.control;

import com.hobomaster.gallery.db.PersonRepository;
import com.hobomaster.gallery.db.entities.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PersonController {

    private final PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/persons")
    public ResponseEntity<Page<Person>> persons(@PageableDefault Pageable pageable) {
        Page<Person> s = personRepository.findAll(pageable);
        return ResponseEntity.ok(s);
    }

    @PutMapping("/persons")
    public ResponseEntity<List<Person>> persons(@RequestBody List<Person> persons) {
        List<Person> s = personRepository.saveAll(persons);
        return ResponseEntity.ok(s);
    }

    @DeleteMapping("/persons")
    public ResponseEntity persons(Long[] ids) {
        personRepository.deleteInBatch(Arrays.stream(ids)
                .map(id -> Person.builder().id(id).build()).collect(Collectors.toList())
        );
        return ResponseEntity.ok().build();
    }
}
