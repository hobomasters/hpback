package com.hobomaster.gallery.control

import com.fasterxml.jackson.databind.ObjectMapper
import com.hobomaster.gallery.db.entities.Person
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import java.time.LocalDate
import java.util.stream.Stream

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@SpringBootTest
@AutoConfigureMockMvc
class PersonControllerTest extends Specification {
    @Autowired
    MockMvc mvc

    @Autowired
    ObjectMapper objectMapper

    def "get persons"() {
        when:
        def response = mvc.perform(get("/persons"))
                .andReturn()

        then:
        response != null
    }

    def "save and delete persons"() {
        given:
        def persons = [
                new Person(null, 'Ivan', 'Ivanovich', 'Ivanov', LocalDate.of(1990, 01, 12)),
                new Person(null, 'Пётр', 'Петрович', 'Петров', LocalDate.of(1980, 01, 12))
        ]

        when:
        def response = mvc.perform(put("/persons")
                .content(objectMapper.writeValueAsString(persons))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn().response

        then:
        response.status == 200
        def ids = objectMapper.readValue(response.contentAsString, Person[].class).collect {it.id}

        when:
        response = mvc.perform(delete("/persons").param("ids", ids[0] as String, ids[1] as String))
                    .andReturn().response
        then:
        response.status == 200
    }

}